import express,{Request,Response} from 'express';

export const isLoggedIn = (req:Request,res:Response,next:express.NextFunction) => {

    if (req.session['user']){
        next();
    } else{
        res.redirect('/');
    }
}

export const isLoggedInAPI = (req:Request,res:Response,next:express.NextFunction) => {
    if (req.session['user']){
        next();
    } else {
        res.status(401).json({message:"Unauthorized"});
    }
}