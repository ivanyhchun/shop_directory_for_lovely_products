import multer from 'multer';
import path from 'path';
import express from 'express';
import expressSession from 'express-session';
import { Client, QueryResult } from 'pg';
import http from 'http';
import dotenv from 'dotenv';
import { checkPassword, hashPassword } from './hash';
import { isLoggedInAPI } from './guard';

// configure file storage path for upload

dotenv.config();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve(`./public/asset`));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
});

const sessionMiddleware = expressSession({
    secret: 'Toys bring you joy',
    resave: true,
    saveUninitialized: true
});

const upload = multer({ storage });
const app = express();
const server = new http.Server(app);

const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

client.connect();

app.use(sessionMiddleware);
app.use(express.urlencoded({ extended: true }));        // must add this code to enable the server to read Form URL Encoded (e.g. for input login user name or password)
app.use(express.json());             // must add this code to enable the server to read JSON and convert to object (without using JSON.parse)


app.use((req, res, next) => {

    req.session['visited'] = 1;
    next();
});


app.get('/over18', (req,res) => {
    req.session["over18"]=true;
    res.json({})
})
app.get('/isOver18', (req,res) => {
    res.json({over18:req.session["over18"]});
})

app.get('/current-user', async function (req,res){
    const userFound = req.session['user'];
    if (userFound) {
        res.json({
            loggedIn: true,
            username:userFound.username,
            id:userFound.id
        })
    } else {
        res.json({loggedIn:false})
    }
})

app.get('/posting-user', isLoggedInAPI, async function (req, res) {
    try {
        const userFound = req.session['user'];
        if (userFound) {
            res.json({
                username: userFound.username,
                id: userFound.id
            });
        } else {
            res.status(401).json({ message: "unauthorized" });
        }
    } catch (e) {
        console.log(e)
    }
})

app.post('/login', async (req, res) => {
    try {
        var users = (await client.query(/*sql*/`SELECT * from users where username = $1`, [req.body.username])).rows;
        const userFound = users[0];
        // check if username exist
        if (!userFound) {
            res.status(401).json({ success: false, message: "Incorrect Username/Password" });
            return;
        }
        // check if password match with database hashed string
        const match = await checkPassword(req.body.password, userFound.password);
        if (match) {
            console.log(userFound)
            req.session['user'] = userFound;
            res.status(200).json({ success: true }).redirect('./public/member.html')
        } else {
            res.status(401).json({ success: false, message: "Incorrect Username/Password" });
        }

    } catch (err) {
        console.log(err);
    }
})


app.post('/signup', async (req, res) => {
    try {
        let user = {
            username: req.body.username,
            password: await hashPassword(req.body.password)
        }
        const check = (await client.query(/*sql*/`SELECT users.username FROM users where username = $1`,[req.body.username])).rows
        console.log(check)
        if (check.length == 0){
            await client.query(/*sql*/`INSERT INTO users (username, password, name, created_at) 
                VALUES ($1, $2, $3, NOW());`, [user.username, user.password,user.username]);
            // immediately fetch the server to obtain the id assigned to this new user
            const result = (await client.query(/*sql*/`SELECT * from users where username = $1`, [req.body.username])).rows;
            req.session['user'] = result[0]
            res.status(200).json(result[0]);
        } else {
            res.status(401).json({ success: false, message: "Member with same user name already exist." })
        }
    } catch (err) {
        console.log(err);
    }
})

app.get('/directory', async (req, res) => {

    const query = req.query;
    let input = {
        searchText: "",
        districts: [],
        category: [],
        page:0
    }
    if (query["input"]) {
        input = JSON.parse(query["input"].toString())
    }
    // console.log(input)
    let searchText = input.searchText;
    const d_IDs = input.districts;
    const c_IDs = input.category;
    const page=input.page;
    // console.log(d_IDs)
    let searchTextCondition = "";
    let districtCondition = "";
    let categoryCondition = "";
    let index = 1;
    let districtItems: string[] = [];
    let categoryItems: string[] = [];
    let searchQuery = `SELECT shops.*, max(category.id) AS category_ID, max(district.district) AS district FROM shops
        INNER JOIN shops_category on shops.id = shops_category.shop_id 
        INNER JOIN district ON shops.district_id = district.id 
        INNER JOIN category ON category.id = shops_category.category_id `; 
    // if(!req.session["page"])req.session["page"]=0;
    let limitQuery = `GROUP BY shops.id ORDER BY shops.id OFFSET ${page*9} LIMIT 9`; 

    if (searchText) {
        searchTextCondition += `lower(district) like lower($${index}) OR lower(shops.name) like lower($${index}) OR lower(category.name) like lower($${index})`;
        index++;
        searchText = "%" + searchText + "%";
    }

    if (d_IDs.length > 0) {
        if (searchText) {
            searchTextCondition += " OR ";
        }
        for (let d_ID of d_IDs) {
            districtCondition += `district.id = $${index++}`;
            districtItems.push(d_ID);
            if (d_ID < Math.max(...d_IDs)) {
                districtCondition += " OR ";
            }
        }
    }

    if (c_IDs.length > 0) {
        if (d_IDs.length == 0 && searchText) {
            searchTextCondition += " OR ";
        } else if (d_IDs.length > 0) {
            districtCondition += " OR ";
        }
        for (let c_ID of c_IDs) {
            categoryCondition += `category.id = $${index++}`;
            categoryItems.push(c_ID);
            if (c_ID < Math.max(...c_IDs)) {
                categoryCondition += " OR ";
            }
        }
    }

    if(searchTextCondition!=""||districtCondition!=""||categoryCondition!="")
        searchQuery+=" WHERE "
    let sqlQuery = `${searchQuery} ${searchTextCondition} ${districtCondition} ${categoryCondition} ${limitQuery}`;
    console.log(sqlQuery, [searchText].concat(districtItems).concat(categoryItems));
    //searchDistrictQuery+=" WHERE "
    // let condition=""
    // let index=1
    // let items:string[]=[]
    // for(let district of districts){
    //   if(condition!="")
    //   condition+=" OR ";
    //   condition+=`lower(district)=$${index++}`
    //   items.push(district)
    // }
    //searchDistrictQuery+=condition

    //let district_ids=await client.query(searchDistrictQuery,items);
    //searchQuery=`SELECT * FROM shops WHERE `
    let result;
    if (searchText) {
        result = await client.query(sqlQuery, [searchText].concat(districtItems).concat(categoryItems));
    } else {
        result = await client.query(sqlQuery, districtItems.concat(categoryItems));
    }

    // console.log(input)



    // const result = await client.query(/*sql*/`SELECT shops.*, category.name AS category_name, district.district from shops INNER JOIN shops_category ON shops.id = shops_category.shop_id INNER JOIN district ON shops.district_id = district.id INNER JOIN category ON category.id = shops_category.category_id where lower(district) like lower($1) OR lower(shops.name) like lower($1) OR lower(category.name) like lower($1)`, ['%' + req.query.input + '%']); 


    res.json(result.rows);
})


app.get('/shops', async (req, res) => {
    res.sendFile(path.resolve('./public/shop_page.html'))
})

app.get('/shop-info', async (req, res) => {
    try{
        const id = req.query.id
        const result = await client.query(/*sql*/`SELECT shops.*, category.name AS category_name 
            FROM shops 
            INNER JOIN shops_category ON shops.id = shops_category.shop_id 
            INNER JOIN category ON category.id = shops_category.category_id 
            WHERE shops.id = $1`, [id]);
        res.json(result);
    } catch (e) {
        console.log(express)
    }
})

app.get('/shop-reviews', async (req, res) => {
    try {
        const id = req.query.id
        const result = await client.query(/*sql*/`SELECT users.name AS member_name,users.profile_image, reviews.*
            FROM reviews
            INNER JOIN users on users.id = reviews.user_id
            INNER join shops on shops.id = reviews.shop_id
            WHERE shops.id = $1
            ORDER by reviews.updated_at DESC
        `, [id])
        res.json(result.rows);

    } catch (e) {
        console.log(e)
    }
})


app.post('/reviews', upload.single('image'), async function (req, res) {
    try {
        const review = req.body;
        let result: QueryResult<any>
        // check if submitted review contain image
        if (req.file) {
            // check if submitted review has specified rating
            if (review.rating != 'null') {
                result = await client.query(/*sql*/`INSERT INTO reviews (shop_id,user_id,comment,image,rating,created_at) 
                    VALUES ($1,$2,$3,$4,$5,NOW())`, [review.shop_id, review.member_id, review.content, req.file.filename, review.rating])
            } else {
                result = await client.query(/*sql*/`INSERT INTO reviews (shop_id,user_id,comment,image,created_at) 
                    VALUES ($1,$2,$3,$4,NOW())`, [review.shop_id, review.member_id, review.content, req.file.filename])
            }
        } else {
            if (review.rating != 'null') {
                result = await client.query(/*sql*/`INSERT INTO reviews (shop_id,user_id,comment,rating,created_at) 
                    VALUES ($1,$2,$3,$4,NOW())`, [review.shop_id, review.member_id, review.content, review.rating])
            }
            else {
                result = await client.query(/*sql*/`INSERT INTO reviews (shop_id,user_id,comment,created_at) 
                    VALUES ($1,$2,$3,NOW())`, [review.shop_id, review.member_id, review.content])
            }
        }
        res.json({ success: true })
    }
    catch (e) {
        console.log(e)
    }
})

app.get('/promoted-shops', async (req, res) => {
    try{
        const result = await client.query(/*sql*/`SELECT * from shops where is_promoted = true`);
        res.json(result.rows);
    } catch (e) {
        console.log(e)
    }
})

app.get('/member', async (req, res) => {
    res.sendFile(path.resolve('./public/member.html'))
})

app.get('/member-info', async (req, res) => {
    try {
        const id = req.query.id
        // if (req.session['user']) {
            const result = await client.query(/*sql*/ `
            SELECT users.name,users.profile_image,users.created_at 
                FROM users
                WHERE users.id = $1
            `, [id]);
            res.json(result.rows)
        // } else {
        //     res.status(401).json({ success: false, message: "not logged in" })
        // }
    } catch (e) {
        console.log(e)
    }
})

app.post('/member-info/:id', upload.single('image'), async (req,res)=>{
    try {
        console.log(req.body)
        console.log(req.file.filename)
        const id = parseInt(req.params.id);
        if (req.file){
            await client.query(/*sql*/`
                UPDATE users SET name = $1, profile_image = $2 where id = $3`,[req.body.name,req.file.filename,id])
        } else {
            await client.query(/*sql*/`
                UPDATE users SET name = $1 where id = $2`,[req.body.name,id])
        }
        res.status(200).json({success:true})
    } catch (e) {
        console.log(e)
    }
})



app.get('/member-review', async (req, res) => {
    try {
        const id = req.query.id
        // if (req.session['user']) {
            const result = await client.query(/*sql*/ `
            SELECT users.id,shops.id, shops.name AS shop_name, shops.chi_name AS shop_chi_name,reviews.* 
                FROM reviews 
                INNER JOIN users on users.id = reviews.user_id
                INNER join shops on shops.id = reviews.shop_id
                WHERE users.id = $1
                ORDER by reviews.updated_at DESC 
            `, [id]);    // memberId defined by client-side .js file.
            res.json(result.rows);
        // } else {
        //     res.status(401).json({ success: false, message: "not logged in" })
        // }
    } catch (e) {
        console.log(e)
    }
})

app.put('/member/review/:id', async (req, res) => {
    try {
        const id = parseInt(req.params.id);
        if (isNaN(id)) {
            res.status(400).json({ message: "id is not an integer" });
            return;
        }
        await client.query(/*sql*/`
            UPDATE reviews SET comment = $1, updated_at = NOW() where id = $2`, [req.body.content, id]);
        res.json({ success: true })
    } catch (e) {
        console.log(e)
    }
});


app.delete('/member/review/:id', async (req, res) => {
    try{
        console.log("delete")
        const id = parseInt(req.params.id);
        if (isNaN(id)) {
            res.status(400).json({ message: "id is not an integer" });
            return;
        }
        await client.query(/*sql*/`
            DELETE FROM reviews where id = $1`, [id]);
        res.json({ success: true })
    } catch (e){
        console.log(e)
    }
})



app.use(express.static('public'));
app.use(express.static('public/asset'));

app.use((req, res) => {
    res.sendFile(path.resolve('./public/404.html'))
})

server.listen(8080, () => {
    console.log("Listening at http://localhost:8080")
});