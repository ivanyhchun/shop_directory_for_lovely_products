# Shop Directory for Lovely Product

# Introduction

This is my first learning project to familiarize with web programming skills (HTML, CSS, JS). Cooperated with my classmate.
Regarding the name of this project, we named in this way to hide our real intention when submitting project idea to our instructors. 

# Readme

The following node modules are recommended to run the code:

npm install ts-node typescript @types/node

npm install ts-node-dev

npm install express-session @types/express-session

npm install express @types/express

npm install body-parser @types/body-parser

npm install jsonfile @types/jsonfile

npm install multer @types/multer

npm install pg @types/pg dotenv @types/dotenv

npm install xlsx @types/xlsx

npm install node-fetch @types/node-fetch

npm install bcryptjs @types/bcryptjs

# Folder Structure

-Mother Directory
--database (to upload any temporary xlsx database here for testing)
-- public (to save .html, .js, .css and images which are supposed to be public)
-- protected (to save pages that are exclusive to administrator)

# Database
Please create a database in your local machine and config the .env file. For example, if I have created a database called "project" with username "user" and password "user", my .env file shall look like:

DB_NAME=project

DB_USERNAME=user

DB_PASSWORD=user

You can configure and run "import.ts" to import district, shops and users database from excel spreadsheet.
