export let responseObject = {
    1:null,
    2:null,
    3:null,
    4:null
};
export let id = window.location.search;
export let recommendationMainBody = document.querySelector('.recommendation-main-body');
const reviewContainer = document.querySelector('.shop-pg-review-sub-container');


export async function loadSearchResult() {
    const urlParams = window.location.search;
    //console.log(urlParams);

    const res = await fetch('/directory' + urlParams);
    const result = await res.json();

    console.log(result);
    if(result.length < 9) {
        document.querySelector('#nextPage').style = 'display: none; '; 
    }
    let checkId = 0; 

    for(let i = 0; i < result.length; i++) {
        if(result[i].id != checkId) {
            document.querySelector('.directory-main-body')
            .innerHTML += `
            <div class="shop-card" data-index="${result[i].id}">
                <div class="shop-photo">
                    <img src="${result[i].image}" alt="" class="img-fluid img-fill">
                </div>
                 <div class="shop-info-body">
                    <div class="shop-info-main">
                        <div class="shop-name">${result[i].name}</div>
                        <div class="shop-location">
                            <i class="fas fa-map-marker-alt location-icon"></i>
                            <div class="shop-district">${result[i].district}</div>
                        </div>
                    </div>
                </div>
            </div>
            `
        }
        checkId = result[i].id; 
    }

    const searchShops = document.querySelectorAll('.shop-card');

    for (let searchShop of searchShops) {
        searchShop.addEventListener('click', async (event) => {
            const shopId = event.currentTarget.getAttribute('data-index');
            console.log(shopId);


            window.location = `/shops?id=${shopId}`;
        })
    }
}


export async function recommend(){
    let category =[];
    recommendationMainBody.innerHTML = ``
    
    if (responseObject[1] != null && responseObject[2] != null && responseObject[3] != null && responseObject[4] != null){
        if (responseObject[4] && !((responseObject[1] && responseObject[2] && !responseObject[3]) || (!responseObject[1] && !responseObject[3]))) {
            category.push({
                id: 5, 
                path:'asset/buttPlug_1.jpg',
                name:'Butt Plug'
                })
        }        
        if (responseObject[2] && responseObject[4] && (responseObject[1] != responseObject[3])) {
            category.push({
                id: 4, 
                path:'asset/cockRing_1.jpg',
                name:'Cock Ring'
                })
        }
        if (responseObject[3] && (responseObject[1] || responseObject[2])) {
            category.push({
                id: 7, 
                path:'asset/condom_1.jpg',
                name:'Condom'
                })
        }
        if (responseObject[3] && !(responseObject[1] && !responseObject[2])) {
            category.push({
                id: 6, 
                path:'asset/dentalDam_1.jpg',
                name:'Dental Dam'
                })
        }
        if (responseObject[1] && responseObject[2] && !responseObject[3]) {
            category.push({
                id: 3, 
                path:'asset/masturbationCup_1.jpg',
                name:'Masturbation Cup'
                })
        }
        if ((responseObject[1] != responseObject[4]) && !((responseObject[1] && responseObject[2] && !responseObject[3] && !responseObject[4]) || (!responseObject[1] && responseObject[2] && responseObject[3] && responseObject[4]))) {
            category.push({
                id: 1, 
                path:'asset/dildo_1.jpg',
                name:'Dildo'
                })
        }
        if (!responseObject[4] && ((responseObject[1] && responseObject[2] && responseObject[3]) || !responseObject[1])) {
            category.push({
                id: 2, 
                path:"asset/vibrator_1.jpg",
                name:'Vibrator'
                })
        }

        console.log(category); 

        for (let index in category) {
            recommendationMainBody.innerHTML += `
            <div class = 'recommendation-card'>
                <div class = "recommendation-photo" data-index="${category[index].id}">
                    <div class="photo-border">
                        <img src=${category[index].path} alt="" class="img-fluid img-fill">
                    </div>
                    <div class = "recommendation-body">
                        <div class = "recommendation-name">${category[index].name}</div>
                    </div>
                </div>
            </div>
            `
        }
    }
    const products = document.querySelectorAll('.recommendation-photo'); 
    for(let product of products) {
        product.addEventListener('click', (event) => {
            const id = event.currentTarget.getAttribute('data-index'); 
            
            window.location = `./directory.html?input={"searchText":"","districts":[],"category":["${id}"],"page":0}`; 
        })
    }
}

export function updateResponse(event){
    const questionID = event.currentTarget.getAttribute('question-id');
    const response = (event.currentTarget.getAttribute('response') == 'true');  // to detect if the responded string is "true", then convert to boolean value
    responseObject[questionID] = response
}
export async function userPostReview() {
    const submitReview = document.querySelector('#review-form')
    const res = await fetch ('/posting-user');
    const result = await res.json();
    const shopId = (window.location.search).slice(4)
    if (res.status === 200){
        let currentUser = result;
        //console.log(currentUser);
        submitReview.style = "display:block;";
        document.querySelector('#review-form')
            .addEventListener('submit',async function(event){
            event.preventDefault();
            const form = event.target;
            const formData = new FormData();
            formData.append('rating',form.rating.value);
            formData.append('content',form.content.value);
            formData.append('member_id',currentUser.id);
            formData.append('shop_id',shopId);
            if (form.image.files[0]){
                formData.append('image',form.image.files[0]);
            }
            const res = await fetch (`/reviews`,{
                method:"POST",
                body:formData
            });
            if (res.status === 200){
                console.log(await res.json());
            }
            form.reset();
            await userPostReview();
            await loadShopReviews();
})
    }
    
}
export async function loadMemberInfo(urlParams,currentUser) {
    const res = await fetch (`/member-info/${urlParams}`);
    let result = await res.json();
    let id = urlParams.slice(4)
    let memberCreateDate = new Date(result[0].created_at).toLocaleDateString();
    let currentUserIsThatMember = (id == currentUser.id)
    if (currentUserIsThatMember) {
        document.querySelector('.member-info').innerHTML = `
        <div class = 'welcome-container'>
            <h2>Hello <span style="color: #ff00ff">${result[0].name}</span></h2>
        </div>
        <div class = 'info-title'>
                <h5>Member info</h5>
        </div>
        <div class = 'info-image'>
                <img src = './${result[0].profile_image? result[0].profile_image : "no_profile.jpg"}'>
        </div>
        <form id = "member-form" action = "/member-info" method = 'POST' enctype="multipart/form-data">
            <div>Upload new profile image here
            </div>
            <input type = 'file' name = 'image' class = 'form-control'>
            <div class = 'info-name'>Display Name: <span id = 'name' contenteditable>${result[0].name}</span></div>
            <div>
                <input type='submit' value='Submit' id='submit-button' class="btn btn-primary" />
            </div>
        </form> 
            <div>
                Joined at: ${memberCreateDate}
            </div>      
            `
        submitMemberNewInfo(id,currentUser);
    } else {
        document.querySelector('.member-info').innerHTML = `       
            <div class = 'info-title'>
                <h5>Member info</h5>
            </div>
            <div class = 'info-image'>
                <img src = './${result[0].profile_image? result[0].profile_image : "no_profile.jpg"}'>
            </div>
            <div class = 'info-name'>
                Name: ${result[0].name}
            </div>
            <div>
                Joined at: ${memberCreateDate}
            </div>      
        `
    }
}

async function submitMemberNewInfo(id,currentUser) {
    document.querySelector('#member-form').addEventListener('submit',async function(event){
        event.preventDefault();
        const display_name = document.querySelector('#name').innerHTML;
        console.log(display_name)
        const form = event.target;
        const formData = new FormData();
        formData.append('name',display_name);
        if (form.image.files[0]){
            formData.append('image',form.image.files[0]);
        }
        console.log(formData)
        const res = await fetch (`/member-info/${id}`,{
            method:"POST",
            body:formData
        });
        if (res.status === 200){
            console.log(await res.json());
        }
        form.reset();
        let urlParams = "?id=" + id;
        await loadMemberInfo(urlParams,currentUser)
    })
     
}

export async function loadMemberReview(member_id,currentUser) {
    const res = await fetch (`/member-review/${member_id}`);
    let result = await res.json();
    let id = member_id.slice(4);
    let currentUserIsThatMember = (id == currentUser.id)
    //const counter = document.querySelector('.review-count-container')

    document.querySelector('.review-main-container').innerHTML=`<div class="review-count-container">
    </div>`;
    if (currentUserIsThatMember){
        document.querySelector('.review-count-container').innerHTML = `
        You have posted ${result.length? result.length : "0"} reviews since joined membership.
        `
    } else {
        document.querySelector('.review-count-container').innerHTML = `
        This member has posted ${result.length? result.length : "0"} reviews since joined membership.
        `
    }
    await loadIndividualMemberReviews(result,currentUserIsThatMember,member_id,currentUser)

}

async function loadIndividualMemberReviews(reviewArray,currentUserIsThatMember,member_id,currentUser){
    if (currentUserIsThatMember){
        for (let review of reviewArray) {
            let createDate = new Date(review.created_at).toLocaleDateString();
            let updateDate = new Date(review.updated_at).toLocaleDateString();
            document.querySelector('.review-main-container').innerHTML += `
                <div class = 'review-container'>
                    <div class = "shop-sub-container">
                        <div class = 'shop-name'>
                            <a href = './shops?id=${review.shop_id}'>${review.shop_chi_name? review.shop_chi_name:review.shop_name}</a>
                        </div>
                        ${review.rating ? 
                        `<div class = 'shop-rating'>
                            Rate: ${review.rating}
                        </div>` 
                        : "Not rated"}
                    </div>
                    <div class = 'review-sub-container'>
                        <div class = 'review-text' contenteditable>
                            ${review.comment}
                        </div>
                        ${review.image? `
                        <div class = 'review-image' >
                            <img src = './${review.image}'>
                        </div>` 
                        : ""}                                   
                        <div class = "date-bar">
                            <div>Review posted at ${createDate}</div>
                            ${review.updated_at? ` <div>&nbspLast updated at ${updateDate}</div>`: ""}
                        </div>            
                    </div>
                    <div class = 'edit-button' review-id = ${review.id}>
                        <i class="fas fa-pencil-alt"></i>
                    </div>
                    <div class = 'delete-button' review-id = ${review.id}>
                        <i class="far fa-trash-alt"></i>
                    </div>
                </div>
            `
        }
    } else {
        for (let review of reviewArray) {
            let createDate = new Date(review.created_at).toLocaleDateString();
            let updateDate = new Date(review.updated_at).toLocaleDateString();
            document.querySelector('.review-main-container').innerHTML += `
                <div class = 'review-container'>
                    <div class = "shop-sub-container">
                        <div class = 'shop-name'>
                            <a href = './shops?id=${review.shop_id}'>${review.shop_chi_name? review.shop_chi_name:review.shop_name}</a>
                        </div>
                        ${review.rating ? 
                        `<div class = 'shop-rating'>
                            Rate: ${review.rating}
                        </div>` 
                        : "Not rated"}
                    </div>
                    <div class = 'review-sub-container'>
                        <div class = 'review-text' contenteditable>
                            ${review.comment}
                        </div>
                        ${review.image? `
                        <div class = 'review-image' >
                            <img src = './${review.image}'>
                        </div>` 
                        : ""}                                   
                        <div class = "date-bar">
                            <div>Review posted at ${createDate}</div>
                            ${review.updated_at? ` <div>&nbspLast updated at ${updateDate}</div>`: ""}
                        </div>            
                    </div>
                </div>
            `
        }
    }
    
    // edit-button function
    editIndividualMemberReview(member_id,currentUser);

    //delete-button function
    deleteIndividualMemberReview(member_id,currentUser)
}

async function editIndividualMemberReview(member_id,currentUser) {
    const editButtons = document.querySelectorAll('.edit-button');
    for (let editButton of editButtons) {
        editButton.addEventListener('click',async function (event) {
            const id = event.currentTarget.getAttribute('review-id');
            const content = event.currentTarget.closest('.review-container').querySelector('.review-sub-container .review-text').innerHTML
            const res = await fetch (`/member/review/${id}`,{
                method:"PUT",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    content:content
                })
            })
            const result = await res.json();
            if (res.status == 200 & result.success){
                await loadMemberReview(member_id,currentUser);
            } else {
                alert ("update failed" + result.message);
            }
        })
    } 
}

async function deleteIndividualMemberReview(member_id,currentUser) {
    const deleteButtons = document.querySelectorAll('.delete-button');
    for (let deleteButton of deleteButtons) {
        deleteButton.addEventListener('click',async function (event){
            const id = event.currentTarget.getAttribute('review-id');
            const res = await fetch(`/member/review/${id}`,{
                method:"DELETE"
            })
            const result = await res.json();
            if (res.status == 200 & result.success){
                await loadMemberReview(member_id,currentUser);
            }else {
                alert ("Delete failed " + result.message)
            }
        })
    }
}

export async function loadPromotedShops() {
    const res = await fetch('/promoted-shops');
    const promotedShops = await res.json();

    for (let promotedShop of promotedShops) {
        document.querySelector('.shop-main-body').innerHTML += `
                <div class="shop-container">
                    <div class="shop-photo shop-photo-1"  data-index="${promotedShop.id}">
                        <img src="${promotedShop.image}" alt="" class="img-fill">
                        <div class="shop-info">
                            <h4>${promotedShop.name}</h4>
                        </div>
                    </div>
                </div>
        `
    }
    
    const shops = document.querySelectorAll('.shop-photo');
    for(let shop of shops) {
        shop.addEventListener('click', (event) => {
            const id = event.currentTarget.getAttribute('data-index'); 
            console.log(id); 

            window.location = `./shops?id=${id}`; 
        })
    }
}

export async function loadShopReviews(){
    let res = await fetch(`shop-reviews/${id}`);
    let results = await res.json();
    let totalRating = 0;
    let validCount = 0;

    reviewContainer.innerHTML = '';
    for (let result of results) {
        if (result.rating != null) {
            validCount ++;
            totalRating += result.rating
        }
    }
    if (validCount >0) {
        let avgRating = Math.round((totalRating/validCount)*10) / 10
        reviewContainer.innerHTML +=`
        <div class = 'shop-rating-container'>
            Shop Average Rating is ${avgRating}.
        </div>`
    } else {
        reviewContainer.innerHTML +=`
        <div class = 'shop-rating-container'>
            This shop has no rating yet.
        </div>`
    }
    await loadIndividualShopReviews(results);    
}

async function loadIndividualShopReviews(resultsArray){
    if (resultsArray.length > 0){
        for (let result of resultsArray) {
            reviewContainer.innerHTML += `
            <div class = 'review-container'>
                <div class = 'member-sub-container'>
                    <div class = "member-name">
                        <a href = '/member?id=${result.user_id}'>${result.member_name}</a>
                    </div>
                    <div class = "profile-image">
                        <img src = './${result.profile_image? result.profile_image : "no_profile.jpg"}'>            
                    </div>
                    <div class = 'posted-at'>
                        ${result.updated_at?
                            `Updated at : ${new Date (result.updated_at).toLocaleDateString()}`:`Posted at : ${new Date (result.created_at).toLocaleDateString()}`
                        }
                    </div>
                </div>        
                <div class = 'review-sub-container'>
                ${result.rating ? `
                    <div class = "review-rating">
                        Rating: ${result.rating}
                    </div>` : ""}
                    <div class = 'review-text'>
                        ${result.comment}
                    </div>
                    ${result.image? `
                        <div class = "review-image">
                            <img src = './${result.image}'>            
                        </div>`:""}
                </div>   
            </div>
            `
        }
    } else {
        reviewContainer.innerHTML += `This shop has no review yet.`
    }
}