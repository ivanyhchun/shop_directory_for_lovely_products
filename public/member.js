import { loadMemberInfo, loadMemberReview, id } from './lib.js';
import './login.js';
import './search.js';
let currentUser;

async function main() {
    const res = await fetch('/current-user');
    currentUser = await res.json();
    await loadMemberInfo(id, currentUser);
    await loadMemberReview(id, currentUser);
}

main()


const logoText = document.querySelectorAll('.logo-text')
for (let text of logoText) {
    text.addEventListener('click', (event) => {
        window.location = './';
    })
}