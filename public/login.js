let loginContainer = document.querySelector('.login-container');
let signupContainer = document.querySelector('.signup-container');

export let loginClicked = false;
export let signupClicked = false; 
let result;

async function main() {
    const res = await fetch ('/current-user');
    result = await res.json();
    return result; 
}

main();



document.querySelector('.user-login')
    .addEventListener('click', (event) => {
        if (result.loggedIn){
            window.location = `/member?id=${result.id}`;
        } else {
            if (!loginClicked) {
                loginClicked = true;
                loginContainer.style = 'display: block; ';
            } else {
                loginClicked = false;
                loginContainer.style = 'display: none; ';
            }
            if(signupClicked) {
                signupContainer.style = 'display: none; '; 
                signupClicked = false; 
            }
        } 
    })

document.querySelector('.sign-up')
    .addEventListener('click', (event) => {
        if(!signupClicked) {
            signupClicked = true;
            signupContainer.style = 'display: block; ';
            loginContainer.style = 'display: none; '; 
        } else {
            signupClicked = false; 
        }
    })

document.querySelector('.login-here')
    .addEventListener('click', (event) => {
        if(signupClicked) {
            signupContainer.style = 'display: none; '; 
            loginContainer.style = 'display: block; animation-name: does-not-exist; '; 
            signupClicked = false; 
        }
    })

document.querySelector('.login-body')
    .addEventListener('submit', async (event) => {
        event.preventDefault();

        const form = event.target;

        const formObject = {};

        formObject.username = form.username.value;
        formObject.password = form.password.value;

        const res = await fetch('/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formObject)
        })
        if (res.status == 200) {
            const result = await res.json();
            location.reload();
            console.log(result);
        } else {
            alert("Wrong Username / Password!")
        }
        form.reset();
    })

document.querySelector('.signup-body')
    .addEventListener('submit', async (event) => {
        event.preventDefault(); 
        const form = event.target; 
        const formObject = {}; 

        formObject.username = form.username.value; 
        formObject.password = form.password.value; 
        console.log(formObject);
        const res = await fetch('/signup', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify(formObject)
        })
        if(res.status == 200) {
            const result = await res.json(); 
            console.log(result); 
            location.reload();
        } else {
            alert("Member with same user name already exist. Please choose another user name");
            form.reset();
        }
        
    })