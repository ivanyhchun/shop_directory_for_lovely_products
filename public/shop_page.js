import { userPostReview, loadShopReviews, id } from './lib.js'
import './login.js'
import './search.js'


async function loadShopInfo() {

    let res = await fetch(`shop-info/${id}`);
    let result = (await res.json()).rows;
    // console.log(result);
    let productString = [];
    for (let shop of result) {
        productString.push(shop.category_name);
    }

    document.querySelector('.shop-pg-main-container').innerHTML += `
            <div class="shop-pg-photo-container">
                <div class="photo-pg-body">
                    <img src="${result[0].image}" alt="" class="img-fluid">
                </div>
                <div class="shop-pg-name">
                    <div>${result[0].name}</div>
                </div>
            </div>
            <div class="shop-pg-info-container">
                <div class="shop-pg-info-body">
                    ${result[0].address ? `<div class="shop-pg-address">
                        <div class="shop-pg-icon">
                            <i class="fas fa-map-marker-alt align-icon"></i>
                        </div>
                        ${result[0].address}
                    </div>` : ""}
                    <div class="shop-pg-phone">
                        <div class="shop-pg-icon">
                            <i class="fas fa-phone align-icon"></i>
                        </div>
                        ${result[0].telephone}
                    </div>
                    ${result[0].about ? `<div class="shop-pg-about">
                        <h4 class="shop-pg-subheader">About ${result[0].name}: </h4>
                        ${result[0].about}
                    </div>` : ""}
                    <div class="shop-pg-productCat">
                        <h5 class="shop-pg-subheader">Product Category: </h5>
                        ${productString.join(", ")}
                    </div>
                    ${result[0].opening_hour ? `<div class="shop-pg-hour">
                        <h5 class="shop-pg-subheader">Opening Hour: </h5>
                        ${result[0].opening_hour}
                    </div>` : ""}
                    ${result[0].payment_method ? `<div class="shop-pg-payment">
                        <h5 class="shop-pg-subheader">Payment Method: </h5>
                        ${result[0].payment_method}
                    </div>` : ""}             
                    ${result[0].website ? `<div class="shop-pg-website">
                        <div class="shop-pg-icon">
                            <i class="fas fa-globe align-icon"></i>
                        </div>
                        <a href="https://www.${result[0].website}/">Visit ${result[0].name}'s Website</a>
                    </div>` : ""}
                </div>

            <div class="shop-pg-map-container">
                <div id="map"></div>
            </div>
    `

    await loadShopReviews()
    function initMap() {
        var markers = [];
        var infoWindows = [];
        var location;
        var geocoder = new google.maps.Geocoder();

        var marker_config = [{
            address: result[0].address
        }];

        function _geocoder(address, callback) {
            geocoder.geocode({
                address: address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    location = results[0].geometry.location;
                    callback(location);
                }
            });
        }

        _geocoder(result[0].address, function (address) {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: address,
                zoom: 18
            });

            marker_config.forEach(function (e, i) {
                _geocoder(e.address, function (address) {
                    var marker = {
                        position: address,
                        map: map
                    }
                    markers[i] = new google.maps.Marker(marker);
                    markers[i].setMap(map);

                });
            });
        });

    }
    return initMap();
}

async function main() {
    loadShopInfo();
    await userPostReview();
}

main()

const logoText = document.querySelectorAll('.logo-text')
for (let text of logoText) {
    text.addEventListener('click', (event) => {
        window.location = './';
    })
}


