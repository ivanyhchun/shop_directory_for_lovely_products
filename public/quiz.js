import { responseObject, updateResponse, recommendationMainBody, recommend } from './lib.js';
import './login.js';
import './search.js';

let button1A = document.querySelector('#answer-1a');
let button1B = document.querySelector('#answer-1b');
let button2A = document.querySelector('#answer-2a');
let button2B = document.querySelector('#answer-2b');
let button3A = document.querySelector('#answer-3a');
let button3B = document.querySelector('#answer-3b');
let button4A = document.querySelector('#answer-4a');
let button4B = document.querySelector('#answer-4b');
let question2 = document.querySelector('#question-2');
let question3 = document.querySelector('#question-3');
let question4 = document.querySelector('#question-4');
let result = document.querySelector('#result')
let recommendation = document.querySelector('#recommendation')


window.onload = document.getElementById("welcome").scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });

button1A.addEventListener('click', async function (event) {
    updateResponse(event);
    button1A.classList.add("selected");
    button1B.classList.remove("selected");
    question2.style = "display:block";
    question2.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    recommend();
});

button1B.addEventListener('click', async function (event) {
    updateResponse(event);
    button1A.classList.remove("selected");
    button1B.classList.add("selected");
    question2.style = "display:block";
    question2.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    recommend();
});

button2A.addEventListener('click', async function (event) {
    updateResponse(event);
    button2A.classList.add("selected");
    button2B.classList.remove("selected");
    question3.style = "display:block";
    question3.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    recommend();
});

button2B.addEventListener('click', async function (event) {
    updateResponse(event);
    button2A.classList.remove("selected");
    button2B.classList.add("selected");
    question3.style = "display:block";
    question3.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    recommend();
});

button3A.addEventListener('click', async function (event) {
    updateResponse(event);
    button3A.classList.add("selected");
    button3B.classList.remove("selected");
    question4.style = "display:block";
    question4.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    recommend();
});

button3B.addEventListener('click', async function (event) {
    updateResponse(event);
    button3A.classList.remove("selected");
    button3B.classList.add("selected");
    question4.style = "display:block";
    question4.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    recommend();
});

button4A.addEventListener('click', async function (event) {
    updateResponse(event);
    button4A.classList.add("selected");
    button4B.classList.remove("selected");
    result.style = "display:block";
    recommendation.style = "display:flex";
    recommend();
    result.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
});

button4B.addEventListener('click', async function (event) {
    updateResponse(event);
    button4A.classList.remove("selected");
    button4B.classList.add("selected");
    result.style = "display:block";
    recommendation.style = "display:flex"
    recommend();
    result.scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
});

const logoText = document.querySelectorAll('.logo-text')
for (let text of logoText) {
    text.addEventListener('click', (event) => {
        window.location = './';
    })
}