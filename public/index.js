import {loadPromotedShops} from './lib.js'
import './login.js'
import './search.js' 

let entryClicked = false;
let warningContainer = document.querySelector('.warning-container'); 
let containerOverlay = document.querySelector('.container-overlay'); 
async function checkOver18(){
    const result= await fetch("/isOver18");
    const json=await result.json();
    console.log(json)
    if(json.over18){
        warningContainer.style = "display: none; ";
        containerOverlay.style = "display: none; ";
    }else{
        warningContainer.style = "display: block; ";
        containerOverlay.style = "display: block; ";
    }
}
checkOver18();


document.querySelector('.enter-button')
    .addEventListener('click', async (event) => {
        if (!entryClicked) {
            await fetch("/over18");
            warningContainer.style = "display: none; ";
            containerOverlay.style = "display: none; ";
        }
    })

document.querySelector('.exit-button')
    .addEventListener('click', (event) => {
        window.location = 'https://www.google.com';  
    })


loadPromotedShops();

document.querySelector('.img-button').addEventListener('click', (event) => {
    window.location = '/quiz.html';
})