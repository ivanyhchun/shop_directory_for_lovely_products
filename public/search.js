let loginClicked = false;
let signupClicked = false;

let advSearchContainer = document.querySelector('.adv-search-container');


document.querySelector('.search-bar')
    .addEventListener('click', (event) => {
        if (!loginClicked) {
            loginClicked = true;
            advSearchContainer.style = 'display: flex; ';
        } else {
            loginClicked = false;
            advSearchContainer.style = 'display: none; ';
        }
    });


document.querySelector('.search-icon')
    .addEventListener('click', async (event) => {
        event.preventDefault();
        showSearchResult(0);
    });



export function showSearchResult(page) {
    const formObject = {};
    let searchText = document.querySelector('#search-bar').value
    let districtBoxes = [...document.querySelectorAll('.checkbox')];
    let filteredDistrict = districtBoxes.filter(elem => {
        return elem.checked;
    }).map(elem => {
        return elem.name;
    })

    let catBoxes = [...document.querySelectorAll('.cat-box')];
    let filteredCategory = catBoxes.filter(elem => {
        return elem.checked;
    }).map(elem => {
        return elem.name;
    })
    formObject.searchText = searchText;
    formObject.districts = filteredDistrict;
    formObject.category = filteredCategory;
    formObject.page = page;
    let input = JSON.stringify(formObject)
    window.location = `/directory.html?input=${input}`;
}