import xlsx from 'xlsx';
import {Client} from 'pg';
import dotenv from 'dotenv';


// Configure the file path of the relevant excel file here.
const shop_data_excel = './database/project_1_shop_data.xlsx'
const district_excel = './database/project_1_district_data.xlsx'
const user_excel = './database/user.xlsx'
const reviews_excel = './database/reviews.xlsx'
const category_excel = './database/category.xlsx'
const shops_category_excel = './database/project_1_shops_category_data.xlsx'

export interface User {
    name:string,
    email:string,
    username:string,
    password:string,
    profile_image:string,
    created_at:string
}

export interface District {
    district:string
}

export interface Category{
    name:string,
    description:string,
    created_at:string,
    updated_at:string,
    image:string
}

export interface Shop {
    name:string,
    chi_name:string,
    about:string,
    address:string,
    telephone:string,
    email:string,
    website:string,
    district_id:number,
    opening_hour:string,
    payment_method:string,
    has_online_shop:boolean,
    has_physical_store:boolean,
    image:string,
    is_promoted:boolean
}

export interface Shops_category {
    category_id:number, 
    shop_id:number
}

export interface Reviews {
    shop_id:number,
    user_id:number,
    comment:string,
    image:string,
    created_at:string,
    updated_at:string,
    rating:number
}

dotenv.config();
export const client = new Client({
    database:process.env.DB_NAME,
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD
});
client.connect();


async function importDistrict(){
    try {
        const workbook = await xlsx.readFile(district_excel);
        const worksheets = workbook.SheetNames;
        const entries: District[] = await xlsx.utils.sheet_to_json(workbook.Sheets[worksheets[0]]);

        for (let entry of entries){
            // prepared statement + bindings
            await client.query(`insert into district (district) values ($1);`,[entry.district])
        }
    // const result = await client.query('SELECT * from district');
    // const usersFromDB = result.rows;
    // console.log(usersFromDB);
    } catch (e) {
        console.log(e)
    }    
}

async function importCategory(){
    try {
        const workbook = await xlsx.readFile(category_excel);
        const worksheets = workbook.SheetNames;
        const entries: Category[] = await xlsx.utils.sheet_to_json(workbook.Sheets[worksheets[0]]);

        for (let entry of entries){
            // prepared statement + bindings
            await client.query(`insert into category (name,description,created_at,updated_at,image) values ($1,$2,$3,$4,$5);`,[entry.name,entry.description,entry.created_at,entry.updated_at,entry.image])
        }
    const result = await client.query('SELECT * from category');
    const usersFromDB = result.rows;
    console.log(usersFromDB);
    } catch (e) {
        console.log(e)
    }    
}

async function importShops(){
    try {
        const workbook = await xlsx.readFile(shop_data_excel);
        const worksheets = workbook.SheetNames;
        const entries: Shop[] = await xlsx.utils.sheet_to_json(workbook.Sheets[worksheets[0]]);
        // console.log(entries); 

        for (let entry of entries){
            // prepared statement + bindings
            await client.query(`insert into shops (name,chi_name,about,address,telephone,email,website,district_id,opening_hour,payment_method,has_online_shop,has_physical_store,image, is_promoted) 
                values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);`,[entry.name,entry.chi_name,entry.about,entry.address,entry.telephone,entry.email,entry.website,entry.district_id,entry.opening_hour,entry.payment_method,entry.has_online_shop,entry.has_physical_store,entry.image,entry.is_promoted])
        }
    // const result = await client.query('SELECT * from shops');
    // const usersFromDB = result.rows;
    // console.log(usersFromDB);
    } catch (e) {
        console.log(e)
    }    
}

export async function importShopsCategory() {
    try {
        const workbook = await xlsx.readFile(shops_category_excel); 
        const worksheets = workbook.SheetNames; 
        const entries: Shops_category[] = await xlsx.utils.sheet_to_json(workbook.Sheets[worksheets[0]]); 
        // console.log(entries); 

        for(let entry of entries) {
            await client.query(/*sql*/`INSERT INTO shops_category (category_id, shop_id) VALUES ($1, $2)`, [entry.category_id,entry.shop_id]); 
        }
    } catch(e) {
        console.log(e); 
    }
}

async function importUsers(){
    try {
        const workbook = await xlsx.readFile(user_excel);
        const worksheets = workbook.SheetNames;
        const entries: User[] = await xlsx.utils.sheet_to_json(workbook.Sheets[worksheets[0]]);

        for (let entry of entries){
            // prepared statement + bindings
            await client.query(`insert into users (name,email,username,password,profile_image,created_at) values ($1,$2,$3,$4,$5,$6);`,[entry.name,entry.email,entry.username,entry.password,entry.profile_image,entry.created_at])
        }
    // const result = await client.query('SELECT * from users');
    // const usersFromDB = result.rows;
    // console.log(usersFromDB);
    } catch (e) {
        console.log(e)
    }    
}

async function importReviews(){
    try {
        const workbook = await xlsx.readFile(reviews_excel);
        const worksheets = workbook.SheetNames;
        const entries: Reviews[] = await xlsx.utils.sheet_to_json(workbook.Sheets[worksheets[0]]);

        for (let entry of entries){
            // prepared statement + bindings
            await client.query(`insert into reviews (shop_id,user_id,comment,image,created_at,updated_at,rating) values ($1,$2,$3,$4,$5,$6,$7);`,[entry.shop_id,entry.user_id,entry.comment,entry.image,entry.created_at,entry.updated_at,entry.rating])
        }
    // const result = await client.query('SELECT * from reviews');
    // const usersFromDB = result.rows;
    // //console.log(usersFromDB);
    } catch (e) {
        console.log(e)
    }    
}


// Shops must be imported after District, otherwise error may occur when attempt to import a shops where the corresponding district is not yet inserted in database
async function main() {
    
    await importDistrict();
    await importCategory();
    await importShops();
    await importUsers();
    await importReviews();
    await importShopsCategory()
}

console.log("Import from .xlsx in progress. Terminate this program if you are not intended to do so.")
main();