-- Create Table: district, category, users, shops, reviews, shops_category, users_recommendation
-- the sequence matter as table with dependency on foreign key must be created afterwards.
CREATE TABLE district (
    id SERIAL PRIMARY KEY,
    district VARCHAR(255)
);

CREATE TABLE category (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    description TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    image VARCHAR(255)
);

CREATE TABLE users (
    id SERIAL primary key,
    name VARCHAR(255),
    email VARCHAR(255),
    username VARCHAR(255),
    password VARCHAR(255),
    profile_image VARCHAR(255),
    created_at TIMESTAMP
);

CREATE TABLE shops (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    chi_name VARCHAR(255),
    about VARCHAR(255),
    address TEXT,
    telephone VARCHAR(255),
    email varchar(255),
    website VARCHAR(255),
    district_id integer,
    FOREIGN KEY (district_id) REFERENCES district(id),
    opening_hour VARCHAR(255),
    payment_method TEXT,
    has_online_shop BOOLEAN,
    has_physical_store BOOLEAN,
    image VARCHAR(255),
    is_promoted BOOLEAN DEFAULT FALSE
);

CREATE TABLE reviews (
    id SERIAL PRIMARY KEY,
    shop_id integer,
    FOREIGN KEY (shop_id) REFERENCES shops(id),
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    comment TEXT,
    image VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    rating INTEGER
);

CREATE TABLE shops_category (
    id SERIAL PRIMARY KEY,
    category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES category(id),
    shop_id INTEGER,
    FOREIGN KEY (shop_id) REFERENCES shops(id)
);

CREATE TABLE users_recommendation (
    id SERIAL PRIMARY KEY,
    category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES category(id),
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id)
);



SELECT * from users_recommendation; 
select * from shops;
select * from reviews;
select * from category;
select * from district;
select * from users;

SELECT * from shops inner join district on shops.district_id = district.id where district like '%Cent%' OR name like '%Cent%';

-- load all reviews made by a specific member
SELECT users.name,users.profile_image,users.created_at AS member_created, shops.name AS shop_name, reviews.* from reviews 
    INNER JOIN users on users.id = reviews.user_id
    INNER join shops on shops.id = reviews.shop_id
    where users.id = '1' 
    ORDER by reviews.created_at DESC;
-- load all reviews made to a specific shop
SELECT users.name AS member_name, reviews.*
    from reviews
    INNER JOIN users on users.id = reviews.user_id
    INNER join shops on shops.id = reviews.shop_id
    where shops.id = '1'
    ORDER BY reviews.updated_at DESC;
-- load member info 
SELECT users.name,users.profile_image,users.created_at AS member_created,shops.name AS shop_Name, reviews.*
    from users
    LEFT OUTER join reviews on users.id = reviews.user_id
    LEFT OUTER join shops on shops.id = reviews.shop_id
    where users.id = '1';

SELECT * from users where username = 'ivan'

SELECT * from shops_category;

SELECT shops.*, category.name AS category_name, district.district FROM shops
    INNER JOIN shops_category
        ON shops.id = shops_category.shop_id
    INNER JOIN district
        ON shops.district_id = district.id
    INNER JOIN category
        ON category.id = shops_category.category_id
    WHERE district.id = '1'; 

SELECT shops.*, category.name FROM category
    INNER JOIN shops_category
        ON category.id = shops_category.category_id
    INNER JOIN shops
        ON shops.id = shops_category.shop_id
    WHERE category.id = '1';

SELECT shops.*, category.name FROM shops
    INNER JOIN shops_category
        ON shops.id = shops_category.shop_id
    INNER JOIN category
        ON category.id = shops_category.category_id
    WHERE category.id = '1';    


select * from shops 
SELECT DISTINCT shops.*, max(category.id) AS category_ID,  max(district.district) FROM shops
        INNER JOIN shops_category on shops.id = shops_category.shop_id 
        INNER JOIN district ON shops.district_id = district.id 
        INNER JOIN category ON category.id = shops_category.category_id
        GROUP BY shops.id ORDER BY shops.id ;
    

UPDATE users SET password = '$2a$10$M.Q6bwJPkkwSkGRhCYHN/Odu/YWJfCvvMbrYypk/bHJignR1QDvIK' WHERE username = 'ivan';
UPDATE users SET password = '$2a$10$/aFNLtOVAWA7UZeUsm1G4O6m9xANS9rhmZBvBbe1r8t6ZfyLrNFCa' WHERE username = 'tab';
UPDATE users set password = '$2a$10$WeGS/f1gI.ZvX3V8XSQ/lujzyWv4sj7Sw40ps7SU9JhaGm1JDQc1u' where name = 'Gordon';
UPDATE users set username = 'gordon' where name = 'Gordon';


-- For removing all tables in case of error
-- drop table shops_category; <--- please don't drop this table!!
drop table users_recommendation;
DROP TABLE shops_category; 
drop table reviews;
drop table shops;
drop table users;
drop table category;
drop table district;
